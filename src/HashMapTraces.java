import java.util.HashMap;

public class HashMapTraces extends MapTraces{

	public HashMapTraces(){
		super();
		initialiser();
	}
	
	public Traces extract(String ssid) {
		return traces.get(ssid);
	}

	public void initialiser() {
		traces=new HashMap<String,Traces>();
	}
}
