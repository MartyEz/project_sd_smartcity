import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;


public abstract class AbstractTraces {
	
	public abstract void ajouter(Trace tr);
	
	public abstract String toString();
	
	public abstract Traces extract(String ssid);
	
	public abstract int taille();
	
	public void load(String filename,String filename2,int validlimit) throws IOException{
		
		double wificnt=0;
		double validwifi=0;
		Scanner SLine = null;
		String ligne;
		String[] splited;
		HashMap<String,Gps> gpsmap;
		HashMap<String,HashSet<String>> alreadyCheck;
		gpsmap=new HashMap<String,Gps>();
		alreadyCheck=new HashMap<String,HashSet<String>>();
		
		
		try {
			SLine = new Scanner(new FileReader(new File(filename2)));
			ligne = SLine.nextLine();
			while ((SLine.hasNextLine())) {
				
				ligne = SLine.nextLine();
				splited=ligne.split(",");
				splited[0]=splited[0].substring(0, 9);
				gpsmap.put(splited[0],new Gps(Float.parseFloat(splited[1]),Float.parseFloat(splited[2])));

			}
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		
		finally {
            if (SLine != null) {
            	SLine.close();
            }
		}
		
		

		try {
			SLine = new Scanner(new FileReader(new File(filename)));
			ligne = SLine.nextLine();
			while ((SLine.hasNextLine())) {
				boolean lock=false;
				wificnt++;
				ligne = SLine.nextLine();
				splited=ligne.split(",");
				splited[0]=splited[0].substring(0, 9);
				
				if(gpsmap.containsKey(splited[0]) && (splited[2].compareTo("<hidden>")!=0)){
					
					if(!alreadyCheck.containsKey(splited[0])){
						HashSet<String> ssid2 = new HashSet<String>();
						ssid2.add(splited[2]);
						alreadyCheck.put(splited[0], ssid2);
					}
					else{
						if(alreadyCheck.get(splited[0]).contains(splited[2])){
							lock=true;
						}
						else
							alreadyCheck.get(splited[0]).add(splited[2]);
					}
					if(!lock){
					
					ajouter(new Trace(splited[0],splited[2],Integer.parseInt(splited[5]),gpsmap.get(splited[0])));
					
					validwifi++;
					//System.out.println("MATCH");
					}
				}

			}
			//System.out.println((validwifi/wificnt)*100+" "+validlimit);
			if((validwifi/wificnt)*100<validlimit)
				throw new IOException("Not enought valid wifi info to continue."+(validwifi/wificnt)*100);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		
		finally {
            if (SLine != null) {
            	SLine.close();
            }
		}
		
		//System.out.println(Runtime.getRuntime().freeMemory());
	}
	
	public void save(String filename){
		
		File file = new File(filename);
		/*try{
		if(file.exists())
			throw new FileAlreadyExistsException("File already Exist");
		}
		catch (FileAlreadyExistsException e){
			e.printStackTrace();
		}*/
		Writer SLine = null;
		
			try {
				SLine = new FileWriter(filename);
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}
		
		
		try {
			SLine.write(toString());
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		try {
			SLine.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
}
