import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class QuizzCar{
	
	
	public static void main(String[]args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrez le nom de fichier des traces wifi");
		String namewifi=sc.nextLine();
		System.out.println("Entrez le nom de fichier des traces gps");
		String namegps=sc.nextLine();
		System.out.println("Entrez le % acceptable de traces non comformes");
		int seuil=sc.nextInt();
		sc.nextLine();
		AbstractTraces att = new TreeTraces();
		try {
			att.load(namewifi, namegps, seuil);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean quit=false;
		while(!quit){
			System.out.println("0 - Recheche ssid avec pr�dictions\n1 - Graph et recherche de chemin\n2 - Quit");
			int choix=sc.nextInt();
			sc.nextLine();
			if(choix==0){
				System.out.println("Entrez un ssid � rechercher");
				String ssid=sc.nextLine();
				Traces traces = att.extract(ssid);
				if(traces == null){
					if(((TreeTraces) att).extractAll(((TreeTraces) att).getRoot(),ssid,0).getSon()!=null){
						((TreeTraces) att).suggest(((TreeTraces) att).extractAll(((TreeTraces) att).getRoot(),ssid,0).getSon(),ssid);
						ArrayList<String> suu = ((TreeTraces) att).getSuggest();
						
						if(suu==null)
							System.out.println("Le ssid entr� n'existe pas aucun autre ssid ne lui ressemble");
						else{
							System.out.println("Le ssid entr�s n'�xiste pas, voice les propositions :");
							for(String s : suu){
								System.out.println(s);
							}
						}
						((TreeTraces) att).delSugg();
					}
					else
						System.out.println("Le ssid entr� n'existe pas aucun autre ssid ne lui ressemble");
				}
				else{System.out.println(traces.toString());
				System.out.println("Il y a "+traces.taille()+" correspondant � "+ssid);
				}
			}
			else if(choix==1){
				System.out.println("Veillez entrer la distance de fussion des traces pour un m�me ssid (par default 150 , port�e d'un wifi)");
				int rangeMerge;
				try
				{
				rangeMerge=sc.nextInt();
				}
				catch(InputMismatchException exception)
				{
					System.out.println("Set default 150");
					rangeMerge=150;
				}
				
				sc.nextLine();
				
				
				int rangelink=300;
				System.out.println("Choisir m�thode la m�thode de cr�ation de lien entre les AP :\n0-Rayon d'action calcul�\n1-Distance fixe");
				int choixMet=sc.nextInt();
				sc.nextLine();
				if(choixMet==1){
					System.out.println("Veuillez entrer la distance limite qui cr�e un lien entre 2 AP (par default 300 : 200+200)");
					
					try
					{
					rangelink=sc.nextInt();
					}
					catch(InputMismatchException exception)
					{
						System.out.println("Set default 400");
						rangelink=300;
					}
					sc.nextLine();
				}
				
				
				
				
				
				
				
				
				((TreeTraces) att).filter(((TreeTraces) att).getRoot(),rangeMerge);
				Traces tr2 = ((TreeTraces) att).createTraces();
				Sommet somtab[] = new Sommet[tr2.taille()];
				for(int i=0;i<somtab.length;i++){
					somtab[i]= new Sommet(((ArrayList<Trace>) tr2.getCollect()).get(i));
				}
				for(int i=0;i<somtab.length;i++){
					for(int y=0;y<somtab.length;y++){
						if(somtab[i]!=somtab[y]){
							if(choixMet==1){
								//if(somtab[i].getValue().dist(somtab[y].getValue())<=somtab[i].getRayon()+somtab[y].getRayon()){
								if(somtab[i].getValue().dist(somtab[y].getValue())<=rangelink){
									somtab[i].addLink(somtab[y], somtab[i].getValue().dist(somtab[y].getValue()));
								}
							}
							else{
								if(somtab[i].getValue().dist(somtab[y].getValue())<=somtab[i].getRayon()+somtab[y].getRayon()){
									somtab[i].addLink(somtab[y], somtab[i].getValue().dist(somtab[y].getValue()));
								}
							}
						}
					}	
				}
				
				Graph gr = new Graph();
				for(int i=0;i<somtab.length;i++){
				gr.addSommet(somtab[i]);
				}
				//gr.printSommet();
				
				ArrayList<Sommet> startC;
				ArrayList<Sommet> endC;
				System.out.println("Veuillez entrez ssid du point de d�part");
				String startname=sc.nextLine();
				startC=gr.getSuggestSommet(startname);
				for(Sommet i : startC){
					System.out.println(i);
				}
				System.out.println("Voici les ssid se trouvant dans des zones g�ographiques diff�rentes. Veuillez entrer l'id de votre d�part (premier nombre)");
				System.out.println("#Pour exemples les couples suivants id_debut:id_fin ont des chemins existants. Entrez id_depart mnt et id_fin quand demand�");
				System.out.println("120:81");
				int idstart=sc.nextInt();
				sc.nextLine();
				System.out.println("Veuillez entrez ssid du point d'arriv�e");
				String endname=sc.nextLine();
				endC=gr.getSuggestSommet(endname);
				for(Sommet i : endC){
					System.out.println(i);
				}
				System.out.println("Voici les ssid se trouvant dans des zones g�ographiques diff�rentes. Veuillez entrer l'id de votre arriv�e (premier nombre)");
				int idend=sc.nextInt();
				sc.nextLine();
				
				if(gr.getSommetId(idstart)!=null && gr.getSommetId(idend) != null){
					System.out.println("START : "+gr.getSommetId(idstart));
					System.out.println("END : "+gr.getSommetId(idend));
					
					/*gr.dijkstra(gr.getSommetId(120));
					gr.printTab();*/
					ArrayList<Sommet> path=gr.getPath(gr.getSommetId(idstart), gr.getSommetId(idend));
					System.out.println("THE PATH IS : ");
					for(Sommet x : path){
						System.out.println(x.getId()+" "+x.getValue().toString());
					}
				}
				Sommet.resetCmp();
			}
			else if(choix==2)
				quit=true;
		}
        if (sc != null) {
        	sc.close();
        }
	}
}