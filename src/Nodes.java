
public class Nodes{
	
	private Nodes son;
	private Nodes brother;
	private char letter;
	private Traces traces;
	
	public Nodes(){
		
	}
	
	public Nodes(char c){
		letter=c;
		
	}
	
	public Nodes(Nodes son, Nodes bro, char a){
		son=son;
		brother=bro;
		letter=a;
		
	}
	
	public Nodes(Nodes son, Nodes bro, char a, Traces tra){
		son=son;
		brother=bro;
		letter=a;
		traces=tra;
	}
	
	public void initTraces(){
		traces=new LinkedListTraces();
	}
	

	/**
	 * @return the son
	 */
	public Nodes getSon() {
		return son;
	}

	/**
	 * @param son the son to set
	 */
	public void setSon(Nodes son) {
		this.son = son;
	}

	/**
	 * @return the brother
	 */
	public Nodes getBrother() {
		return brother;
	}

	/**
	 * @param brother the brother to set
	 */
	public void setBrother(Nodes brother) {
		this.brother = brother;
	}

	/**
	 * @return the letter
	 */
	public char getLetter() {
		return letter;
	}

	/**
	 * @param letter the letter to set
	 */
	public void setLetter(char letter) {
		this.letter = letter;
	}

	/**
	 * @return the traces
	 */
	public Traces getTraces() {
		return traces;
	}

	/**
	 * @param traces the traces to set
	 */
	public void setTraces(Traces traces) {
		this.traces = traces;
	}
	
	public boolean hasSon(){
		if(son==null)
			return false;
		else
			return true;
	}
	
	public boolean hasBro(){
		if(brother==null)
			return false;
		else
			return true;
	}
	
	public boolean hasTraces(){
		if(traces==null)
			return false;
		else
			return true;

	}
	
	public void addTrace(Trace tr){
		traces.ajouter(tr);
	}
	
	public static Nodes copy(Nodes a){
		Nodes b= new Nodes();
		b.son=a.getSon();
		b.brother=a.getBrother();
		b.letter=a.getLetter();
		b.traces=a.getTraces();
		return b;
	}
	
	

}
