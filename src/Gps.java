
public class Gps {
	
	private float longitude;
	private float latitude;
	
	public Gps(float lat, float lng){
		this.longitude=lng;
		this.latitude=lat;
		
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	
	public String toString(){
		String rsl="";
		rsl+=this.latitude+","+this.longitude;
		
		return rsl;
	}

}
