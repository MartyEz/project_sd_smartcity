import java.util.HashMap;




public class Sommet {
	private static int cmp=0;
	private Trace valueS;
	private String val;
	private int id;
	private float rayon;
	private HashMap<Sommet,Float> linkedS;
	private float dist;
	private Sommet father;
	
	public Sommet(Trace s){
		valueS=s;
		id=cmp;
		cmp++;
		linkedS=new HashMap<Sommet,Float>();
		rayon=s.getRayon();
	}
	
	public Sommet() {
		linkedS=new HashMap<Sommet,Float>();
	}
	
	public Sommet(String s) {
		linkedS=new HashMap<Sommet,Float>();
		id=cmp;
		cmp++;
		val=s;
	}
	
	public String getStr(){
		return val;
	}
	
	public void setRayon(float r){
		rayon=r;
	}
	
	public float getRayon(){
		return rayon;
	}
	
	public String toString(){
		return getId()+" "+valueS.toString();
	}
	
	public int getId(){
		return id;
	}

	public float getValue(Sommet a){
			return linkedS.get(a); 
	}
	
	public void addLink(Sommet s, float f){
		linkedS.put(s, f);
	}
	
	
	public void addLinkR(Sommet s, float f){
		linkedS.put(s, f);
		s.addLink(this, f);
	}
	
	public float getDist(){
		return dist;
	}
	
	public void setDist(float a){
		this.dist=a;
	}
	
	public Trace getValue(){
		return valueS;
	}
	
	public Sommet getFather(){
		return father;
	}
	
	public void setFather(Sommet a){
		father=a;
	}
	
	public HashMap<Sommet,Float> getLinked(){
		return linkedS;
	}

	public static void resetCmp() {
		cmp=0;
		
	}
	
	

}
