import java.util.ArrayList;


public class Graph {
	private ArrayList<Sommet> sommets;
	private ArrayList<Sommet> checked;
	
	
	public Graph(){
		sommets=new ArrayList<Sommet>();
		checked=new ArrayList<Sommet>();
	}
	
	public void addSommet(Sommet a){
		sommets.add(a);
	}
	
	public Sommet getLower(){
		float min=sommets.get(0).getDist();
		Sommet lower=sommets.get(0);
		for(Sommet b : sommets){
			if(b.getDist()<=min)
				lower=b;
		}
		return lower;
		
	}
	
	public ArrayList<Sommet> getChecked(){
		return checked;
	}
	
	public ArrayList<Sommet> getSuggestSommet(String a){
		ArrayList<Sommet> candidate= new ArrayList<Sommet>();
		for( Sommet s : sommets){
			if(s.getValue().getSsid().compareTo(a)==0){
				candidate.add(s);
			}
		}
		return candidate;
	}
	
	public void printSommet(){
		for(Sommet a : sommets){
			System.out.println(a.getId()+a.getStr()+":"+a.getDist()+a.getFather());
		}
	}
	
	public void printTab(){
		for ( Sommet a : checked){
			System.out.println(a.getId()+" "+a.getValue().toString()+" "+a.getDist()+" "+a.getFather().getId()+" "+a.getFather().getValue().toString());
		}
	}
	
	public void printTab2(){
		for ( Sommet a : checked){
			System.out.println(a.getId()+" "+a.getStr()+" "+a.getDist()+" "+a.getFather().getId()+" "+a.getFather().getStr());
		}
	}
	
	public Sommet getSommetId(int id){
		for(Sommet a : sommets){
			if(a.getId()==id)
				return a;
		}
		return null;
	}
	
	public ArrayList<Sommet> getPath(Sommet a, Sommet b){
		dijkstra(a);
		boolean nopath=false;
		ArrayList<Sommet> path=new ArrayList<Sommet>();
		ArrayList<Sommet> path2=new ArrayList<Sommet>();
		Sommet current=b;
		while(current != a){
			path.add(current);
			if(current.getFather()!=null)
				current=current.getFather();
			else{
				System.out.println("Il n'y a apparemment pas de chemin");
				nopath=true;
				break;
			}
		}
		path.add(a);
		for(int i=path.size()-1;i>=0;i--){
			path2.add(path.get(i));
		}
		if(!nopath)
			return path2;
		else
			return null;
	}
	
	public void updateDist(Sommet a, Sommet b){
			
		if(b.getDist()> a.getDist()+a.getValue(b)){
			b.setDist(a.getDist()+a.getValue(b));
			b.setFather(a);
			if(!checked.contains(b))
				checked.add(b);
		}
		
	}
	
	public void dijkstra(Sommet a){
		for(Sommet b : sommets){
			b.setDist(10000000);
		}
		Sommet start =a;
		start.setDist(0);
		while(sommets.size()!=0){
			Sommet lower = getLower();
			sommets.remove(lower);
			for(Sommet c : lower.getLinked().keySet()){
				if(sommets.contains(c)){
					updateDist(lower,c);
				}
			}
		}
	}
	
	
	public static void main(String[]args){
		Graph gr= new Graph();
		Sommet a = new Sommet("a");
		Sommet b = new Sommet("b");
		Sommet c = new Sommet("c");
		Sommet d = new Sommet("d");
		Sommet e = new Sommet("e");
		Sommet f = new Sommet("f");
		Sommet g = new Sommet("g");
		Sommet h = new Sommet("h");
		a.addLinkR(b, 2);
		a.addLinkR(c, 5);
		a.addLinkR(d, 1);
		b.addLinkR(d, 2);
		b.addLinkR(c, 3);
		d.addLinkR(c, 3);
		d.addLinkR(e, 1);
		c.addLinkR(e,1);
		c.addLinkR(f,5);
		e.addLinkR(f,2);
		g.addLinkR(h,2);
		gr.addSommet(a);
		gr.addSommet(b);
		gr.addSommet(c);
		gr.addSommet(d);
		gr.addSommet(e);
		gr.addSommet(f);
		gr.addSommet(g);
		gr.addSommet(h);
		gr.printSommet();
		ArrayList<Sommet> path=gr.getPath(a, f);
		gr.printTab2();
		System.out.println("----------------------");
		for(Sommet x : path){
			System.out.println(x.getId()+x.getStr());
		}
		System.out.println(f.getDist());
		
	}
}
