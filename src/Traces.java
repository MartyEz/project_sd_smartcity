import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public abstract class Traces extends AbstractTraces{
	
	
protected Collection<Trace> traces;
	
	public Traces(){
		super();
	}
	
	public Traces(Collection<Trace> trs){
		super();
		this.traces = trs;
	}
	
	public abstract Traces extract(String ssid);
	
	public Iterator<Trace> iterator(){
		return traces.iterator();
		
	}
	
	public Collection<Trace> getCollect(){
		return traces;
	}
	
	public void ajouter(Trace tra){
		this.traces.add(tra);
	}
	
	public String toString(){
		//System.out.println(this.traces.size());
		String rsl="";
		for(Trace tr : this.traces){
			rsl+=tr.toString();
			//rsl+=tr.getTimestamp()+" "+tr.getSsid()+"\n";
			//System.out.println(tr.getSsid());
		}
		return rsl;
	}
	
	public int taille(){
		return traces.size();
	}
	
	
	
	public abstract void initialiser();

}
