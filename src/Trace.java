

public class Trace {
	
	private String timestamp;
	private String ssid;
	private int signal;
	private float rayon;
	private Gps gps;
	
	public Trace(String tmp, String ssid, int signal, Gps gps){
		this.timestamp=tmp;
		this.ssid=ssid;
		this.signal=signal;
		this.gps=gps;
	}
	
	public Trace(String string) {
		this.ssid=string;
	}

	public String toString(){
		return this.timestamp+","+this.ssid+","+this.signal+","+this.gps.toString()+"\n";
		
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSsid() {
		return ssid;
	}
	
	public void setRayon(float r){
		rayon=r;
	}
	
	public float getRayon(){
		return rayon;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public int getSignal() {
		return signal;
	}
	
	public float dist(Trace u){
		float a=this.gps.getLatitude();
		float b=u.getGps().getLatitude();
		float c=this.gps.getLongitude();
		float d=u.getGps().getLongitude();
		return (float)(6371000*Math.acos(Math.cos(a*(Math.PI/180))*Math.cos(b*(Math.PI/180))*Math.cos(c*(Math.PI/180)-d*(Math.PI/180))+Math.sin(a*(Math.PI/180))*Math.sin(b*(Math.PI/180))));
	}

	public void setSignal(int signal) {
		this.signal = signal;
	}

	public Gps getGps() {
		return gps;
	}

	public void setGps(Gps gps) {
		this.gps = gps;
	}
	
	

}
