

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner SLine = null;
		String ligne;
		ArrayList<String[]> lines;
		ArrayList<String> linesO;
		lines = new ArrayList<String[]>();
		linesO = new ArrayList<String>();
		boolean timestamp = false;
		boolean ssid = false;
		boolean doubletimestamp=false;
		int timearg = 0;
		
		int ssidarg = 0;

		for (int i = 0; i < args.length; i++) {
			if (args[i].toLowerCase().equals("ssid")) {
				ssid = true;
				ssidarg = i;
			}
			else if (args[i].toLowerCase().equals("timestamp")) {
				timestamp = true;
				timearg = i;
			}
		}
		
		if(args.length==4 && timestamp==true){
			timestamp=false;
			doubletimestamp=true;
			
		}
			

		try {
			SLine = new Scanner(new FileReader(new File(args[args.length - 1])));
			while ((SLine.hasNextLine())) {
				ligne = SLine.nextLine();
				linesO.add(ligne);
				lines.add(ligne.split(","));

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Nom de fichier manquant.");
			e.printStackTrace();
		}

		finally {
            if (SLine != null) {
            	SLine.close();
            }
		}

		String[] a;
		if (timestamp) {
	
			for (int i = 0; i < lines.size(); i++) {
				a = lines.get(i);
				if (a[0].equals(args[timearg + 1])) {
					
					System.out.println(linesO.get(i));
				}
			}
		}
		
		else if (doubletimestamp) {
			for (int i = 0; i < lines.size(); i++) {
				a = lines.get(i);
				if (a[0].compareTo(args[timearg + 1]) >= 0 && a[0].compareTo(args[timearg + 2]) <=0 ) {
					
					System.out.println(linesO.get(i));
				}
			}
		}

		else if (ssid) {
			for (int t = 0; t < lines.size(); t++) {
				a = lines.get(t);
				if (a[2].equals(args[ssidarg + 1])) {
					System.out.println(linesO.get(t));
				}
			}
		}
		
		else{
			for (int i = 0; i < linesO.size(); i++) {
				System.out.println(linesO.get(i));
			}
			
		}

	}

}
