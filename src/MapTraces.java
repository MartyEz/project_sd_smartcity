import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public abstract class MapTraces extends AbstractTraces{
	
	
protected Map<String,Traces> traces;
	
	public MapTraces(){
		super();
	}
	
	public MapTraces(Map<String,Traces> trs){
		super();
		this.traces = trs;
	}
	
	
	
	/*public Iterator<Traces> iterator(){
		return traces.values().iterator();
		
	}*/
	
	public void ajouter(Trace tr){
		Traces trs=new LinkedListTraces();
		if(!(traces.containsKey(tr.getSsid()))){
			
			trs.ajouter(new Trace(tr.getTimestamp() ,tr.getSsid(),tr.getSignal(),tr.getGps()));
			traces.put(tr.getSsid(),trs);
		}
		else
			traces.get(tr.getSsid()).ajouter(new Trace(tr.getTimestamp(),tr.getSsid(),tr.getSignal(),tr.getGps()));
	}
	
	public String toString(){
		//System.out.println(this.traces.size());
		String rsl="";
		for(Traces tr : this.traces.values()){
			for(Trace t : tr.getCollect()){
				rsl+=tr.toString();
			}
		}
		return rsl;
	}
	
	public int taille(){
		return traces.size();
	}
	
	public abstract void initialiser();
	
}
