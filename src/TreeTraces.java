import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreeTraces extends AbstractTraces {
	
	private Nodes root;
	private ArrayList<Character> chare;
	private ArrayList<String> suggest; 
	private ArrayList<Trace> real; 
	private boolean listCheck;
	private int taille;
	
	public TreeTraces(){
		root=new Nodes();
		listCheck=false;
		chare=new ArrayList<Character>();
		suggest=new ArrayList<String>();
		real=new ArrayList<Trace>();
		taille=0;
	}
	
	public Nodes getRoot(){
		return root;
	}
	
	public ArrayList<String> getSuggest(){
		chare.clear();
		return suggest;
	}
	
	public void delSugg(){
		suggest.clear();
	}

	@Override
	public void ajouter(Trace tr) {
		Nodes end = createPath(root,tr.getSsid(),0);
		//System.out.println("returned "+end+" "+end.getLetter());
		if(end.hasTraces()==false){
			end.initTraces();
			taille++;
		}
		end.addTrace(tr);	
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Traces extract(String ssid) {
		Nodes end=createPath(root,ssid,0);
		if(end!=null)
			return end.getTraces();
		else
			return null;
	}

	@Override
	public int taille() {
		// TODO Auto-generated method stub
		return taille;
	}
	
	
	public Nodes extractAll(Nodes n,String ssid, int incr){
		if(n.getLetter()==ssid.charAt(incr)){
			if(incr==ssid.length()-1)
				return n;
			if(n.hasSon())
				return extractAll(n.getSon(),ssid,incr+1);
			else
				return null;
		}
		else{
				if(n.hasBro())
					return extractAll(n.getBrother(),ssid,incr);
				else
					return null;
		}
	}
	
	
	public void suggest(Nodes a,String ssid){
		addSuggest(a.getLetter());
		if(a.hasTraces())
			clearSuggest(ssid);
		if(a.hasSon())
			suggest(a.getSon(),ssid);
		/*else{
			for(char i : chare){
				System.out.println(i);
			}
			System.out.println("CLEAR");
			clearSuggest(ssid);
		}*/
		if(a.hasBro())
			suggest(a.getBrother(),ssid);
	}
	
	
	public void addSuggest(char a){
			chare.add(a);
	}
	
	public void clearSuggest(String ssid){
		//System.out.println("AAA");
		String rsl ="";
		for(char c : chare){
			rsl+=c;
		}
		suggest.add(ssid+rsl);
		
	}
	
	public Traces createTraces(){
		Traces newtr = new ArrayListTraces(real);
		return newtr;
	}

	
	public Nodes createPath(Nodes n,String ssid,int incr){
		if(n.getLetter()==ssid.charAt(incr)){
			if(incr==ssid.length()-1){
				//System.out.println(n +" end of word at "+incr+" for : "+ssid.charAt(incr));
				return n;
			}
			if(n.hasSon()){
				//System.out.println(n +" can continue on son at "+incr+" for : "+ssid.charAt(incr));
				return createPath(n.getSon(),ssid,incr+1);
			}
			else{
				//System.out.println(n +" create rest of word after a son at "+incr+" for : "+ssid.charAt(incr));
				Nodes[] tab = new Nodes[ssid.length()-1-incr];
				for(int i=0;i<tab.length;i++){
					tab[i]=new Nodes(ssid.charAt(incr+i+1));
					//System.out.println(tab[i]+" "+tab[i].getLetter());
				}
				for(int i=0;i<tab.length-1;i++){
					tab[i].setSon(tab[i+1]);
				}
				n.setSon(tab[0]);
				/*Nodes a=null;
				for(int i=0;i<tab.length;i++){
					a=Nodes.copy(tab[i]);
				}
				return a;*/
				return tab[tab.length-1];
			}
		}
		else{
				if(n.hasBro()){
					//System.out.println(n +" i have a bro i : "+incr+" for : "+ssid.charAt(incr));
					return createPath(n.getBrother(),ssid,incr);
				}
				
				else{
					//System.out.println(n +" new bro at i : "+incr+" for : "+ssid.charAt(incr));
					Nodes[] tab = new Nodes[ssid.length()-incr];
					for(int i=0;i<tab.length;i++){
						tab[i]=new Nodes(ssid.charAt(i+incr));
						//System.out.println(tab[i]+" "+tab[i].getLetter());
					}
					for(int i=0;i<tab.length-1;i++){
						tab[i].setSon(tab[i+1]);
					}
					n.setBrother(tab[0]);
					/*Nodes a=null;
					for(int i=0;i<tab.length;i++){
						a=Nodes.copy(tab[i]);
					}
					return a;*/
					return tab[tab.length-1];

				}
		}
		
		
	}
	

	
	public void filter(Nodes a,float dist){
		
		if(a.hasTraces()){
			distFromMax((LinkedList<Trace>) a.getTraces().getCollect(),dist);
		}
		if(a.hasSon())
			filter(a.getSon(),dist);
		if(a.hasBro())
			filter(a.getBrother(),dist);
		
	}
	
	public int distFromMax(LinkedList<Trace> list,float dist){
		
		if(list.size()==0)
			return 0;
		else{
		Trace maxT=list.get(0);
		int max=maxT.getSignal();
		float maxD=0;
		for(int i=0;i<list.size();i++){
			if(list.get(i).getSignal()>=max){
				max=list.get(i).getSignal();
				maxT=list.get(i);
			}
		}
		//System.out.println(maxT);
		
		for(int i=0;i<list.size();i++){
			if(list.get(i) != maxT){
				//System.out.println(maxT.dist(list.get(i))+" "+maxT+" "+list.get(i));
				if(maxT.dist(list.get(i))<dist){
					if(maxT.dist(list.get(i))>=maxD){
						maxD=maxT.dist(list.get(i));
					}
					list.remove(i);
					i=i-1;
				}
			}
			
		}
		
		/*for(int i=0;i<list.size();i++){
			if(list.get(i) != maxT){
				System.out.println(maxT.dist(list.get(i))+" "+maxT+" "+list.get(i));
			}
		}*/
		//System.out.println("Rayon "+maxD);
		maxT.setRayon(maxD);
		real.add(maxT);
		list.remove(maxT);
		//System.out.println(list.size());
		return distFromMax(list,dist);
		}
		//return list;
	}
	
	public static void main(String[]args){
		
		/*Trace tr1= new Trace("EazP");
		Trace tr2= new Trace("EazP");
		Trace tr3= new Trace("10","a",189,new Gps(15.45,48.4));
		Trace tr7= new Trace("1455550","a",18555559,new Gps(1555554.45,4555558.4));
		Trace tr4= new Trace("EazProi");
		Trace tr5= new Trace("EazTze");
		Trace tr6= new Trace("Ea");
		abt.ajouter(tr1);
		abt.ajouter(tr2);
		abt.ajouter(tr3);
		abt.ajouter(tr4);
		abt.ajouter(tr5);
		abt.ajouter(tr6);
		abt.ajouter(tr7);
		abt.suggest(abt.extractAll(abt.root,"E",0).getSon(),"E");
		for(String a : abt.suggest){
			System.out.println(a);
		}*/

		
		TreeTraces abt = new TreeTraces();
		/*AbstractTraces abt = new ArrayListTraces();*/
		try {
			abt.load("w","g",5);

			abt.filter(abt.root,150);
			//System.out.println(abt.real.size());
			Traces tr2 = abt.createTraces();
			//System.out.println(tr2.extract("eduroam"));
			Sommet somtab[] = new Sommet[tr2.taille()];
			for(int i=0;i<somtab.length;i++){
				somtab[i]= new Sommet(((ArrayList<Trace>) tr2.getCollect()).get(i));
			}
			for(int i=0;i<somtab.length;i++){
				for(int y=0;y<somtab.length;y++){
					if(somtab[i]!=somtab[y]){
						if(somtab[i].getValue().dist(somtab[y].getValue())<=400){
							somtab[i].addLink(somtab[y], somtab[i].getValue().dist(somtab[y].getValue()));
						}
					}
					
				}
				
			}
			
			Graph gr = new Graph();
			for(int i=0;i<somtab.length;i++){
			gr.addSommet(somtab[i]);
			}
			//gr.printSommet();
			if(gr.getSommetId(120)!=null && gr.getSommetId(81) != null){
				System.out.println("START : "+gr.getSommetId(120));
				System.out.println("END : "+gr.getSommetId(81));
				
				/*gr.dijkstra(gr.getSommetId(120));
				gr.printTab();*/
				ArrayList<Sommet> path=gr.getPath(gr.getSommetId(120), gr.getSommetId(81));
				if(path!=null){
					System.out.println("THE PATH IS : ");
					for(Sommet x : path){
						System.out.println(x.getId()+" "+x.getValue().toString());
					}
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
}