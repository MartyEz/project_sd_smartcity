
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class LinkedListTraces extends Traces{
	
	
	public LinkedListTraces(){
		super();
		initialiser();
	}
	
	public void initialiser() {
		this.traces = new LinkedList();	
	}

	@Override
	public LinkedListTraces extract(String ssid) {
		long a=System.currentTimeMillis();
		LinkedListTraces rsl=new LinkedListTraces();
		for( Trace tr : traces){
			if(tr.getSsid().compareTo(ssid)==0){
				rsl.ajouter(tr);
			}
		}
		long b=System.currentTimeMillis();
		//System.out.println(b-a);
		return rsl;
	}
}
